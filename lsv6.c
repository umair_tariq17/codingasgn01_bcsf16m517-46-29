#include<unistd.h>
#include<stdio.h>
#include<dirent.h>
#include<string.h>
#include<sys/stat.h>
#include<sys/ioctl.h>
#include<stdlib.h>
#include<pwd.h>
#include<grp.h>
#include<errno.h>
#include<time.h>

extern int errno;
void do_ls(char *);
void do_ls_l(char *);
void print_hyphen_l(char *, char *);
int print_colors(char ,char ,char , char ,char *);
int main(int argc, char* argv[])
{
	if(argc==1)
	{
		do_ls(".");
		exit(1);
	}

	if(strcmp(argv[1],"-l") == 0 && argc == 2)
	{
		printf("LOng listing called\n");
		do_ls_l(".");
		exit(1);	
	}

	else if(strcmp(argv[1],"-l") == 0 && argc > 2)
	{
		int i = 1;
		while(++i<argc)
		{
			printf("Directory listing of %s : \n",argv[i]);
			do_ls_l(argv[i]);
		}
	}

	else
	{
		int i = 0;
		while(++i<argc)
		{
			//printf("Directory listing of %s : \n",argv[i]);
			do_ls(argv[i]);
		}
	}
	return 0;
}

void do_ls(char * dir)
{
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	int rows = w.ws_row;
	int cols = w.ws_col;
	int max = 0;

	//printf("%d %d\n",rows,cols);


	struct dirent * entry;
	char allfiles[25][100];
	DIR *dp = opendir(dir);
	if(dp == NULL)
	{
		fprintf(stderr,"Cannot open directory %s:\n",dir);
		return;
	}

	errno = 0;
	int i = 0;
	while((entry = readdir(dp)) != NULL)
	{
		if(entry == NULL && errno != 0)
		{
			perror("readdir failed");
			exit(errno);
		} 
		else
		{
			if(entry->d_name[0] == '.')
				continue;

			strcpy(allfiles[i],entry->d_name);
			if(strlen(allfiles[i]) > max)
				max = strlen(allfiles[i]);
			i++;
		}
	}
	
	//printf("%d\n",i);
	qsort(allfiles, i , 100, (int (*) (const void *, const void *))strcmp );
	
	char directory[50];
	struct stat info;
	char filetype[100];
	char str[10][100];
	for(int j = 0; j<i ; j++)
	{
		
		strcpy(directory, dir);
		strcat(directory,"/");
		strcat(directory,allfiles[j]);
		//print_hyphen_l(directory,allfiles[j]);

		int rv = lstat(directory,&info);
		if(rv == -1){
			perror("stat failed\n");
			exit(1);
		}
		if((info.st_mode & 0170000) == 0010000)
			filetype[j] = 'p';
		else if((info.st_mode & 0170000) == 0020000)
			filetype[j] = 'c';
		else if ((info.st_mode & 0170000) == 0040000)
			filetype[j] = 'd';
		else if ((info.st_mode & 0170000) == 0060000)
			filetype[j] = 'b';
		else if ((info.st_mode & 0170000) == 0100000)
			filetype[j] = '-';
		else if ((info.st_mode & 0170000) == 0120000)
			filetype[j] = 'l';
		else if ((info.st_mode & 0170000) == 0140000)
			filetype[j] = 's';
		else 
			printf("Unknown file");

		strcpy(str[j],"---------");

		//owner permissions
		if((info.st_mode & 0000400) == 0000400) str[j][0]='r';
		if((info.st_mode & 0000200) == 0000200) str[j][1]='w';
		if((info.st_mode & 0000100) == 0000100) str[j][2]='x';

		//group permissions
		if((info.st_mode & 0000040) == 0000040) str[j][3]='r';
		if((info.st_mode & 0000020) == 0000020) str[j][4]='w';
		if((info.st_mode & 0000010) == 0000010) str[j][5]='x';

		//other permissions
		if((info.st_mode & 0000004) == 0000004) str[j][6]='r';
		if((info.st_mode & 0000002) == 0000002) str[j][7]='w';
		if((info.st_mode & 0000001) == 0000001) str[j][8]='x';

		//special permissions
		if((info.st_mode & 0004000) == 0004000) 
		{
			if(str[j][2] == 'x')
				str[j][2]='s';
			else
				str[j][2]='S';
		}


		if((info.st_mode & 0002000) == 0002000) 
		{
			if(str[j][5]== 'x')
				str[j][5]='s';
			else
				str[j][5]='S';

		}

		if((info.st_mode & 0001000) == 0001000) 
		{
			if(str[j][8]=='x')
				str[j][8]='t';
			else
				str[j][8]='T';
		}






	}
	

	int max_col = cols / max;
	int flag = 0;
	int terminal_rows = i / max_col;
	//printf("%d",max_col);
	//printf("%d\n",max);
	//printf("%d\n\n",terminal_rows);
	max = max + 1;
	int spacer = 0;
	int mover = 0;
	int linechecker = 0;
	int color = 28;
	char o,g,ot;
	if(terminal_rows > 0)
	{
		int r = 0;
		for(int j = 0,k=1; j<i;j++)
		{
			if(r<terminal_rows)
			{
				if(flag == 0)
				{
					o = str[j][2];
					g = str[j][5];
					ot = str[j][8];
					//printf("%c%c%c",o,g,ot);
					color = print_colors(filetype[j],o,g,ot,allfiles[j]);
					//printf("%d",color);
					printf("\033[%dm%s\033[m\n",color,allfiles[j]);
					if(spacer < strlen(allfiles[j]))
						spacer = strlen(allfiles[j]);

					linechecker = 0;
				}
				else
				{
					printf("\033[%dC",mover);
					//printf("%s\n",allfiles[j]);
					o = str[j][2];
					g = str[j][5];
					ot = str[j][8];
					//printf("%c%c%c",o,g,ot);
					color = print_colors(filetype[j],o,g,ot,allfiles[j]);
					//printf("%d",color);
					printf("\033[%dm%s\033[m\n",color,allfiles[j]);
					if(spacer < strlen(allfiles[j]))
						spacer = strlen(allfiles[j]);

					linechecker = 0;
				}
				r++;
			}

			else
			{
				printf("\033[%dA",r);
				mover = mover + spacer + 2;
				spacer = 0;
				printf("\033[%dC",mover);
				//printf("%s\n",allfiles[j]);
				o = str[j][2];
				g = str[j][5];
				ot = str[j][8];
				//printf("%c%c%c",o,g,ot);
				color = print_colors(filetype[j],o,g,ot,allfiles[j]);
				//printf("%d",color);
				printf("\033[%dm%s\033[m\n",color,allfiles[j]);
				if(spacer < strlen(allfiles[j]))
					spacer = strlen(allfiles[j]);
				r = 0;
				r++;
			
				flag = 1;
				linechecker = 1;
			}
		}
	
		while(r<terminal_rows)
		{
			printf("\n");
			r++;
		}

	}

	else
	{
			mover = 2;
			for(int j = 0,k=1; j<i;j++)
			{
				//printf("%s",allfiles[j]);
				o = str[j][2];
				g = str[j][5];
				ot = str[j][8];
				//printf("%c%c%c",o,g,ot);
				color = print_colors(filetype[j],o,g,ot,allfiles[j]);
				//printf("%d",color);
				printf("\033[%dm%s\033[m",color,allfiles[j]);
				printf("\033[%dC",mover);
				flag = 1;
			}

		if(flag == 1)
			printf("\n");
	}

	closedir(dp);

}

void do_ls_l(char * dir)
{
	
	int i = 0;
	struct dirent * entry;
	DIR *dp = opendir(dir);
	if(dp == NULL)
	{
		fprintf(stderr,"Cannot open directory %s:\n",dir);
		return;
	}

	char allfiles[25][100];
	errno = 0;
	while((entry = readdir(dp)) != NULL)
	{

		if(entry == NULL && errno != 0)
		{
			perror("readdir failed");
			exit(errno);
		} 
		else
		{
			if(entry->d_name[0] == '.')
				continue;
			
			//strcpy(allfiles[i],directory);
			strcpy(allfiles[i],entry->d_name);
			//printf("%s\n",directory);
			//printf("%s\n",entry->d_name);
			i++;
		}
	}

	char directory[50];
	qsort(allfiles, i , 100, (int (*) (const void *, const void *))strcmp );
	for(int j = 0; j<i ; j++)
	{
		strcpy(directory, dir);
		strcat(directory,"/");
		strcat(directory,allfiles[j]);
		print_hyphen_l(directory,allfiles[j]);	
	}

	closedir(dp);

}

void print_hyphen_l(char * fname, char * name)
{
	char * token;
	struct stat info;
	int rv = lstat(fname,&info);
	char filetype;
	if(rv == -1){
		perror("stat failed\n");
		exit(1);
	}

	struct passwd * pwd = getpwuid(info.st_uid);
	struct group * grp = getgrgid(info.st_gid);

	char *p;
	
	p = strtok(ctime(&info.st_mtime),"\n");

	if((info.st_mode & 0170000) == 0010000)
		filetype = 'p';
	else if((info.st_mode & 0170000) == 0020000)
		filetype = 'c';
	else if ((info.st_mode & 0170000) == 0040000)
		filetype = 'd';
	else if ((info.st_mode & 0170000) == 0060000)
		filetype = 'b';
	else if ((info.st_mode & 0170000) == 0100000)
		filetype = '-';
	else if ((info.st_mode & 0170000) == 0120000)
		filetype = 'l';
	else if ((info.st_mode & 0170000) == 0140000)
		filetype = 's';
	else 
		printf("Unknown file");

	char str[10];
	strcpy(str,"---------");

	//owner permissions
	if((info.st_mode & 0000400) == 0000400) str[0]='r';
	if((info.st_mode & 0000200) == 0000200) str[1]='w';
	if((info.st_mode & 0000100) == 0000100) str[2]='x';

	//group permissions
	if((info.st_mode & 0000040) == 0000040) str[3]='r';
	if((info.st_mode & 0000020) == 0000020) str[4]='w';
	if((info.st_mode & 0000010) == 0000010) str[5]='x';

	//other permissions
	if((info.st_mode & 0000004) == 0000004) str[6]='r';
	if((info.st_mode & 0000002) == 0000002) str[7]='w';
	if((info.st_mode & 0000001) == 0000001) str[8]='x';

	//special permissions
	if((info.st_mode & 0004000) == 0004000) 
	{
		if(str[2] == 'x')
			str[2]='s';
		else
			str[2]='S';
	}


	if((info.st_mode & 0002000) == 0002000) 
	{
		if(str[5]== 'x')
			str[5]='s';
		else
			str[5]='S';

	}

	if((info.st_mode & 0001000) == 0001000) 
	{
		if(str[8]=='x')
			str[8]='t';
		else
			str[8]='T';
	}

	if(filetype == 'p')
	{
		printf("%c%s% ld %s %s\t%ld\t%s\t\033[28m%s\n\033[m",filetype,str,info.st_nlink,pwd->pw_name,grp->gr_name,info.st_size,p,name);
	}
	else if(filetype == 'c')
	{
		printf("%c%s% ld %s %s\t%ld\t%s\t\033[7m%s\n\033[m",filetype,str,info.st_nlink,pwd->pw_name,grp->gr_name,info.st_size,p,name);
	}

	else if (filetype == 'd')
	{
		printf("%c%s% ld %s %s\t%ld\t%s\t\033[36m%s\n\033[m",filetype,str,info.st_nlink,pwd->pw_name,grp->gr_name,info.st_size,p,name);
	}

	else if(filetype == 'b')
	{
		printf("%c%s% ld %s %s\t%ld\t%s\t\033[7m%s\n\033[m",filetype,str,info.st_nlink,pwd->pw_name,grp->gr_name,info.st_size,p,name);
	}

	else if(filetype == 'p')
	{
		printf("%c%s% ld %s %s\t%ld\t%s\t\033[28m%s\n\033[m",filetype,str,info.st_nlink,pwd->pw_name,grp->gr_name,info.st_size,p,name);
	}

	else if(filetype == 'l')
	{
		printf("%c%s% ld %s %s\t%ld\t%s\t\033[35m%s\n\033[m",filetype,str,info.st_nlink,pwd->pw_name,grp->gr_name,info.st_size,p,name);
	}
	else if(filetype == 's')
	{
		printf("%c%s% ld %s %s\t%ld\t%s\t\033[28m%s\n\033[m",filetype,str,info.st_nlink,pwd->pw_name,grp->gr_name,info.st_size,p,name);
	}
	else if(filetype == '-')
	{
		token = strstr(name,".gz");
		if(token != NULL)
			printf("%c%s% ld %s %s\t%ld\t%s\t\033[31m%s\n\033[m",filetype,str,info.st_nlink,pwd->pw_name,grp->gr_name,info.st_size,p,name);

		else
		{	
			if(str[2]=='x' || str[2] == 's' || str[5]=='x' || str[5] == 's' || str[8]=='x' || str[8] == 's' || str[2]=='S' || str[5] == 's' || str[8] == 'S' )
				printf("%c%s% ld %s %s\t%ld\t%s\t\033[32m%s\n\033[m",filetype,str,info.st_nlink,pwd->pw_name,grp->gr_name,info.st_size,p,name);

			else
			{
				printf("%c%s% ld %s %s\t%ld\t%s\t\033[28m%s\n\033[m",filetype,str,info.st_nlink,pwd->pw_name,grp->gr_name,info.st_size,p,name);
			}

		}
	}

	


}

int print_colors(char filetype,char o,char g,char ot,char * name)
{
	char * token;
	
	if(filetype == 'p')
	{
		return 28;
	}
	else if(filetype == 'c')
	{
		return 7;
	}

	else if (filetype == 'd')
	{
		return 36;
	}

	else if(filetype == 'b')
	{
		return 7;
	}

	else if(filetype == 'l')
	{
		return 35;
	}
	else if(filetype == 's')
	{
		return 28;
	}
	else if(filetype == '-')
	{
		token = strstr(name,".gz");
		if(token != NULL)
			return 31;

		else
		{	
			if(o=='x' || o == 's' || g=='x' || g == 's' || ot=='x' || ot == 's' || o=='S' || g == 's' || ot == 'S' )
				return 32;
			else
			{
				return 28;
			}
		}
	}

	return 32;
}
















