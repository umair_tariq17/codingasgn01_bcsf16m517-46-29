#include<unistd.h>
#include<stdio.h>
#include<dirent.h>
#include<string.h>
#include<sys/stat.h>
#include<stdlib.h>
#include<pwd.h>
#include<grp.h>
#include<errno.h>
#include<time.h>

extern int errno;
void do_ls(char *);
void do_ls_l(char *);
void print_hyphen_l(char *, char *);
int main(int argc, char* argv[])
{
	if(argc==1)
	{
		do_ls(".");
		exit(1);
	}

	if(strcmp(argv[1],"-l") == 0 && argc == 2)
	{
		printf("LOng listing called\n");
		do_ls_l(".");
		exit(1);	
	}

	else if(strcmp(argv[1],"-l") == 0 && argc > 2)
	{
		int i = 1;
		while(++i<argc)
		{
			printf("Directory listing of %s : \n",argv[i]);
			do_ls_l(argv[i]);
		}
	}

	else
	{
		int i = 0;
		while(++i<argc)
		{
			printf("Directory listing of %s : \n",argv[i]);
			do_ls(argv[i]);
		}
	}
	return 0;
}

void do_ls(char * dir)
{
	struct dirent * entry;
	DIR *dp = opendir(dir);
	if(dp == NULL)
	{
		fprintf(stderr,"Cannot open directory %s:\n",dir);
		return;
	}

	errno = 0;
	while((entry = readdir(dp)) != NULL)
	{
		if(entry == NULL && errno != 0)
		{
			perror("readdir failed");
			exit(errno);
		} 
		else
		{
			if(entry->d_name[0] == '.')
				continue;

			printf("%s\n",entry->d_name);
		}
	}

	closedir(dp);

}

void do_ls_l(char * dir)
{
	struct dirent * entry;
	DIR *dp = opendir(dir);
	if(dp == NULL)
	{
		fprintf(stderr,"Cannot open directory %s:\n",dir);
		return;
	}

	errno = 0;
	while((entry = readdir(dp)) != NULL)
	{
		char directory[50];
		strcpy(directory, dir);

		if(entry == NULL && errno != 0)
		{
			perror("readdir failed");
			exit(errno);
		} 
		else
		{
			if(entry->d_name[0] == '.')
				continue;
			strcat(directory,"/");
			strcat(directory,entry->d_name);
			//printf("%s\n",directory);
			//printf("%s\n",entry->d_name);
			print_hyphen_l(directory,entry->d_name);
		}
	}

	closedir(dp);

}

void print_hyphen_l(char * fname, char * name)
{
	struct stat info;
	int rv = lstat(fname,&info);
	char filetype;
	if(rv == -1){
		perror("stat failed\n");
		exit(1);
	}

	struct passwd * pwd = getpwuid(info.st_uid);
	struct group * grp = getgrgid(info.st_gid);

	char *p;
	
	p = strtok(ctime(&info.st_mtime),"\n");

	if((info.st_mode & 0170000) == 0010000)
		filetype = 'p';
	else if((info.st_mode & 0170000) == 0020000)
		filetype = 'c';
	else if ((info.st_mode & 0170000) == 0040000)
		filetype = 'd';
	else if ((info.st_mode & 0170000) == 0060000)
		filetype = 'b';
	else if ((info.st_mode & 0170000) == 0100000)
		filetype = '-';
	else if ((info.st_mode & 0170000) == 0120000)
		filetype = 'l';
	else if ((info.st_mode & 0170000) == 0140000)
		filetype = 's';
	else 
		printf("Unknown file");

	char str[10];
	strcpy(str,"---------");

	//owner permissions
	if((info.st_mode & 0000400) == 0000400) str[0]='r';
	if((info.st_mode & 0000200) == 0000200) str[1]='w';
	if((info.st_mode & 0000100) == 0000100) str[2]='x';

	//group permissions
	if((info.st_mode & 0000040) == 0000040) str[3]='r';
	if((info.st_mode & 0000020) == 0000020) str[4]='w';
	if((info.st_mode & 0000010) == 0000010) str[5]='x';

	//other permissions
	if((info.st_mode & 0000004) == 0000004) str[6]='r';
	if((info.st_mode & 0000002) == 0000002) str[7]='w';
	if((info.st_mode & 0000001) == 0000001) str[8]='x';

	//special permissions
	if((info.st_mode & 0004000) == 0004000) 
	{
		if(str[2] == 'x')
			str[2]='s';
		else
			str[2]='S';
	}


	if((info.st_mode & 0002000) == 0002000) 
	{
		if(str[5]== 'x')
			str[5]='s';
		else
			str[5]='S';

	}

	if((info.st_mode & 0001000) == 0001000) 
	{
		if(str[8]=='x')
			str[8]='t';
		else
			str[8]='T';
	}

	printf("%c%s% ld %s %s\t%ld\t%s\t%s\n",filetype,str,info.st_nlink,pwd->pw_name,grp->gr_name,info.st_size,p,name);


}











